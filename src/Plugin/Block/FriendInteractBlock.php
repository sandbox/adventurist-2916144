<?php

namespace Drupal\heartbeat\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\file\Entity\File;
use Drupal\heartbeat\Entity\Heartbeat;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FriendInteractBlock' block.
 *
 * @Block(
 *  id = "friend_interact_block",
 *  admin_label = @Translation("Friend interact block"),
 * )
 */
class FriendInteractBlock extends BlockBase {

  private $entityTypeManager;


  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function build() {

    $uid = \Drupal::config('heartbeat_friend_interact.settings')->get('uid');
    $user = $this->entityTypeManager->getStorage('user')->load($uid);

    $userPic = $user->get('user_picture')->getValue();

    if (!empty($userPic) && $profilePic = $user->get('user_picture')->getValue()[0]['target_id']) {
      $pic = File::load($profilePic);

      if ($pic !== NULL) {
        $style = $this->entityTypeManager->getStorage('image_style')
          ->load('thumbnail');
        $rendered = $style->buildUrl($pic->getFileUri());
      }
    }


    $flagMarkup = Heartbeat::flagAjaxBuilder('friendship', $user, \Drupal::service('flag'));

    return [
      '#theme' => 'friend_interaction',
      '#user' => 'This user',
      '#flag' => $flagMarkup,
      '#userPic' => $rendered,
      '#cache' => array('max-age' => 0)
    ];
  }

}
