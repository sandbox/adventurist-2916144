<?php

namespace Drupal\heartbeat\Plugin\EntityReferenceSelection;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides specific access control for the user entity type.
 *
 * @EntityReferenceSelection(
 *   id = "heartbeat:user",
 *   label = @Translation("User selection"),
 *   entity_types = {"user"},
 *   group = "default",
 *   weight = 1
 * )
 */
class KekUserSelection extends UserSelection {

  public function entityQueryAlter(SelectInterface $query) {

    parent::entityQueryAlter($query);

    $friendData = \Drupal::config('heartbeat_friendship.settings')->get('data');
    $mynewvar = $query;

    $query->condition('users.uid', $friendData, 'IN');

    return $query;



  }

}
